# KPGR3 - Task 1

\#java, \#glsl, \#lwjgl, \#shader, #grid, \#per-pixel, \#per-fragment, \#per-vertex, \#attenuation, \#shadow-maps, \#blinn-phong, \#uniform, \#cartesian, \#spherical, \#cylindrical, \#coordinates, \#orthogonal, #perspective, #projection

## Task
-   Vytvořte vertex a index buffer pro uložení geometrie tělesa založené na síti trojúhelníků – grid. Vyzkoušejte implementaci gridu pomoci triangle stripu. Model zobrazte ve formě hran i vyplněných ploch
-   Vytvářená tělesa definujte pomocí parametrických funkcí uvedených například na následujících odkazech.
	- [http://www.math.uri.edu/~bkaskosz/flashmo/tools/parsur/](http://www.math.uri.edu/~bkaskosz/flashmo/tools/parsur/)
	- [http://www.math.uri.edu/~bkaskosz/flashmo/tools/sphplot/](http://www.math.uri.edu/~bkaskosz/flashmo/tools/sphplot/)
	- [http://www.math.uri.edu/~bkaskosz/flashmo/tools/cylin/](http://www.math.uri.edu/~bkaskosz/flashmo/tools/cylin/)
Implementujte alespoň 6 funkcí, dvě v kartézských, dvě ve sférických a dvě v cylindrických souřadnicích. Vždy jedna může být použita z uvedených stránek a druhou „pěknou“ navrhněte. Výpočet geometrie zobrazovaných těles (souřadnic vrcholů) i přepočet na zvolený souřadnicový systém bude prováděn vertexovým programem!
-   Alespoň jednu z funkcí modifikujte v čase pomocí uniform proměnné.
-   Vytvořte vhodné pixelové programy pro zobrazení povrchu těles znázorňující barevně pozici (souřadnici xyz, hloubku), barvu, texturu, normálu a souřadnice do textury. Vhodné pro debugování.
-   Vytvořte vertexový a pixelový program pro zobrazení texturovaného osvětleného povrchu pomoci Blinn-Phong osvětlovacího modelu, všechny složky. Předpokládejte reflektorový zdroj světla a útlum prostředí. Implementujte hladký přechod na okraji reflektorového světla. Pozici zdroje světla ve scéně znázorněte.
-   Pozorovatele ovládejte pomocí kamery myší a WSAD.
-   Umožněte nastavení ortogonální i perspektivní projekce.
-   Na vhodných tělesech znázorněte rozdíl mezi výpočtem osvětlení per vertex a per pixel.
-   Implementujte normal a paralax mapping.
-   Implementujte metodu pro výpočet vržených stínů ShadowMaps. Uvažujte alespoň jeden pohybující se zdroj světla a dvě různá zároveň zobrazená tělesa. Alespoň jedno těleso se musí pohybovat. Pro znázornění vržených stínu vykreslete rovinnou podložku s vypočteným osvětlením.
-   Před odevzdáním si znovu přečtěte pravidla odevzdávání a hodnocení projektů uvedené v Průvodci studiem

## Installing 
There is an attached tutorial JakNaPGRF.pdf. This file will guide you to setup the environment. 

The code is tested on Windows. It **does not work on MacOS**.

##### Main steps (in IntelliJ idea):
- In project structure right-click on the "res" and "shaders" folders and select "Mark Directory as" - "Reources Root"
-  Customize and download LWJGL utils for your platform here: [https://www.lwjgl.org/customize](https://www.lwjgl.org/customize). You will need these modules: LWJGL-core, GLFW, Native File Dialog, Nuklear, OpenGL, stb and Tiny File Dialogs. This source code is guaranteed to run with version 3.2.3.
- Open "File" - "Project Structure" - "Global Libraries". Click on plus (New Global Library) and select Java
- Choose .jar files from unzipped downloaded LWJGL utils.
- Check modules in Project Structure. There should be the added global library in the dependencies. If not, click on the plus in dependencies and select "Library" - "Java" and choose the added library.
- In project structure find "src" - "main" - "App", right click on it and select Run App.main

## Control
- Moving camera: **WASD**, **Shift** - move up, **Ctrl** - move down.
- **M**: Show scene from the light point of view
- **L**: Switching between moving light and camera
- **Q**: Show difference between per-vertex and per-pixel surface computation
- **F**: Switching between rendering front polygons. It´s possible to choose between lines and fill
- **B**: Switching between rendering back polygons. It´s possible to choose between lines and fill
- **R**: Start or stop rotation of torus and movement of light
- **T**: Switching between surface types
- **P**: Switching between perspective and and orthogonal projection
- **C**: Print cameras position to console. Mainly for debugging.
- **O**: Switching between object types
- **H**: Switching between light modes: 1: Blinn-Phong and attenuation is on, 2: Blinn-Phong and attenuation is off
- **E**: Switching between showing and hiding debug views (color texture from the light point of view, depth texture from 
the light point of view, texture)