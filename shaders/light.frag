#version 150
out vec4 outColor; // (always only one) output from fragment shader

void main() {
	outColor = vec4(gl_FragCoord.zzz, 1.0);
}
