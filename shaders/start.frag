#version 150

// Used materials from book: Computer Graphics Programming in OpenGL with JAVA by Scott Gordon and John Glevenger (CGP book)

in vec4 depthTextureCoord;
in vec2 textureCoord;
in vec4 vertexPosition;
in vec3 varyingLightDir; // direction from the vertex to the light source
in vec3 varyingNormal; // vertex normal vector
in vec3 varyingHalfVector; // half vector for Blinn-Phong shading
in float lightDistance; // distance between light and vertex
in vec4 perVertexColor;

uniform sampler2D depthTexture; // depth for recognizing shadows
uniform sampler2D textureBricks;
uniform sampler2D textureSea;
uniform float surfaceType; // ids of the surface types are defined in the SurfaceType enum
uniform float objectType; // ids of the object types are defined in the ObjectType enum
uniform vec3 lightDirection; // direction vector from the light source
uniform float lightCutoffAngle; // cos of the light cutoff angle. The cutoff angle is between 0° and 90°
uniform float lightMode; // 1: Blinn-Phong and attenuation is on, 2: Blinn-Phong and attenuation is off

out vec4 outColor; // (always only one) output from fragment shader

// attenuation parameters
const float constantAttenuation = 1; // should be greater or equal to 1
const float linearAttenuation = 0.15;
const float quadraticAttenuation = 0.2;

// light parameters - CGP book page 160
const float materialShininess = 10;
	// Ambient
const float globalAmbient = 0.6; // in range 0-1
const float materialAmbient = 0.2;
	// Diffuse
const float materialDiffuse = 1;
const float posLightDiffuse = 1;
	// Specular
const float materialSpecular = 1;
const float posLightSpecular = 1;


const float PI = 3.1415;

vec4 objectColor(float objectType) {
	switch (int(objectType)) {
		case 0: return vec4(1, 0, 0, 1); // sombrero
		case 1: return vec4(0, 1, 0, 1); // earring
		case 2: return vec4(0, 0, 1, 1); // waves
		case 3: return vec4(1, 0, 1, 1); // ufo
		case 4: return vec4(1, 1, 0, 1); // glass
		case 6: return vec4(1, 1, 1, 1); // torus
		case 7: return vec4(0, 1, 1, 1); // plane
		default : return vec4(0.0, 0, 0, 1.0);
	}
}

void main() {
	// per vertex texture
	if (objectType == 8) {
		outColor = perVertexColor;
		return;
	}
	// per pixel texture
	if (objectType == 9) {
		outColor = texture(textureBricks, textureCoord);
		return;
	}
	// light object is always just yellow
	if (objectType == 5) {
		outColor = vec4(1, 1, 0, 1);
		return;
	}

	// compute base color without lightning or shadows
	vec4 color;
	if (surfaceType == 0) { // depth color
		color = vec4(gl_FragCoord.zzz, 1);
	} else if (surfaceType == 1) { // each object has its color
		color = objectColor(objectType);
	} else if (surfaceType == 2) { // texture
		if (objectType == 2) { // waves has sea texture
			color = texture(textureSea, textureCoord);
		} else { // every other object has brick texture
			color = texture(textureBricks, textureCoord);
		}
	} else if (surfaceType == 3) { // normal
		color = vec4(normalize(varyingNormal), 1.0);
	} else if (surfaceType == 4) { // coordinate to texture
		color = vec4(textureCoord, 0, 1);
	} else { // xyz position
		color = vec4(vertexPosition.xyz, 1);
	}

	if (lightMode == 2) {
		outColor = color;
		return;
	}

	float attenuation = 1.0 / (constantAttenuation + linearAttenuation * lightDistance + quadraticAttenuation * (lightDistance, lightDistance));

	vec3 L = normalize(varyingLightDir); // direction from the vertex to the light source
	vec3 N = normalize(varyingNormal);
	vec3 H = normalize(varyingHalfVector);

	// get the angle between the light and surface normal:
	float cosTheta = dot(L,N);
	// angle between the view vector and reflected light:
	float cosPhi = dot(H,N);

	// compute ADS contributions (per pixel), and combine to build output color:
	float ambient = globalAmbient * materialAmbient;
	float diffuse = posLightDiffuse * materialDiffuse * max(cosTheta, 0);
	float specular = posLightSpecular * materialSpecular * pow(max(cosPhi, 0), materialShininess * 3);


	// https://learnopengl.com/Lighting/Light-casters - Smooth/Soft edges
	// theta is the angle between normalized direct light direction vector and ligt-to-fragment direction vector
	float theta = dot(L, normalize(-lightDirection));
	// ϵ (epsilon) is the cosine difference between the inner (ϕ) and the outer cone (γ)(ϵ=ϕ−γ). Inner cone (ϕ) is in this case the light cutoff angle.
	float lightCutoff = PI/7;
	float cosInnerCone = cos(lightCutoff);
	float cosOuterCone = cos(lightCutoff + PI/36);
	float epsilon = cosInnerCone - cosOuterCone;
	float intensity = (theta - cosOuterCone) / epsilon;
	intensity = clamp(intensity, 0, 1); // intensity has to be in the range from 0 to 1.
	diffuse  *= intensity;
	specular *= intensity;

	// "z" texture value
	// R, G, i B partitions are the same
	float zLight = texture(depthTexture, depthTextureCoord.xy).r;
	// the constant is bias for removing acne
	float zActual = depthTextureCoord.z - 0.01;
	bool isInShadow = zLight <= zActual;

	if (isInShadow) {
		outColor = ambient * color;
	} else {
		outColor = vec4(ambient + attenuation * (diffuse + specular)) * color;
	}
} 
