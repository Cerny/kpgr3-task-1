#version 150
in vec2 inPosition; // input from the vertex buffer

uniform mat4 view;
uniform mat4 projection;
uniform float objectType; // ids of the object types are defined in the ObjectType enum
uniform float time;
uniform mat4 modelTransformation; // transforms object´s position, size and rotation

const float PI = 3.1415;

// My func 1
vec3 getWavesCartesian(vec2 vec) {
	float s = vec.x;
	float t = vec.y;

	float x = -t;
	float y = s;
	float z = (sin(10 * s + (time / 2)) / 15); // move waves by uniform variable

	return vec3(x, y, z);
}

// My func 2
vec3 getUFOSphere(vec2 vec) {
	float s = (vec.x + 1) * PI; // <-1;1> -> <0;2PI>
	float t = (vec.y + 1) * PI / 2; // <-1;1> -> <0;PI>

	float r = abs(t-PI/2);
	float phi = t*sin(t) + 5;
	float theta = s;

	return vec3(r, phi, theta);
}

// My func 3
vec3 getGlassCylindric(vec2 vec) {
	float t = (vec.x + 1) * PI; // <-1;1> -> <0;2PI>
	float s = (vec.y + 1) * PI; // <-1;1> -> <0;2PI>

	float r = (1+min(sin(t),0))*2;
	float theta = s;
	float z = 3-t;

	return vec3(r, theta, z);
}

vec3 getSombreroCylindric(vec2 vec) {
	float t = (vec.x + 1) * PI; // <-1;1> -> <0;2PI>
	float s = (vec.y + 1) * PI; // <-1;1> -> <0;2PI>

	float r = t;
	float theta = s;
	float z = 2 * sin(t);

	return vec3(r, theta, z);
}

vec3 getEarringSphere(vec2 vec) {
	float s = (vec.x + 1) * PI; // <-1;1> -> <0;2PI>
	float t = (vec.y + 1) * PI / 2; // <-1;1> -> <0;PI>

	float r = abs(t-PI/2);
	float phi = t;
	float theta = s;

	return vec3(r, phi, theta);
}

vec3 getSphere(vec2 vec) {
	float s = PI * 0.5 - PI * ((vec.y + 1) / 2); // <-1;1> -> <PI/2;-PI/2>
	float t = (vec.x + 1) * PI; // <-1;1> -> <0;2PI>
	float r = 0.5;

	float x = r * sin(t) * cos(s);
	float y = r * cos(t) * cos(s);
	float z = r * sin(s);

	return vec3(x, y, z);
}

vec3 getTorus(vec2 vec) {
	float s = vec.x * PI; // <-1;1> -> <-PI;PI>
	float t = vec.y * PI; // <-1;1> -> <-PI;PI>

	float x = (3 * cos(s) + cos(t) * cos(s)) / 4;
	float y = (3 * sin(s) + cos(t) * sin(s)) / 4;
	float z = sin(t) / 4;
	return vec3(x, y, z);
}

vec3 sphereToCartesian(vec3 sphereCoodinations) {
	float r = sphereCoodinations.x;
	float phi = sphereCoodinations.y;
	float theta = sphereCoodinations.z;

	float x = r * sin(phi) * cos(theta);
	float y = r * sin(phi) * sin(theta);
	float z = - r * cos(phi);
	return vec3(x, y, z);
}

vec3 cylindricToCartesian(vec3 sphereCoodinations) {
	float r = sphereCoodinations.x;
	float theta = sphereCoodinations.y;
	float cylinZ = sphereCoodinations.z;

	float x = r * cos(theta);
	float y = r * sin(theta);
	float z = cylinZ;
	return vec3(x, y, z);
}

// Generic function
vec3 getObjectCartesian(float objectType, vec2 position) {
	switch (int(objectType)) {
		case 0: return cylindricToCartesian(getSombreroCylindric(position)) / 10;
		case 1: return sphereToCartesian(getEarringSphere(position)) / 2;
		case 2: return getWavesCartesian(position) / 2;
		case 3: return sphereToCartesian(getUFOSphere(position)) / 3;
		case 4: return cylindricToCartesian(getGlassCylindric(position)) / 6;
		case 5: return getSphere(position); // light
		case 6: return getTorus(position);
		default: return vec3(position, 0); // plane
	}
}

vec3 getObjectCartesianTransformed(float objectType, vec2 position) {
	return (modelTransformation * vec4(getObjectCartesian(objectType, position), 1)).xyz;
}

void main() {
	// grid has range <0;1>, transform it to <-1;1>
	vec2 inPosition = inPosition * 2.0 - 1.0;
	vec3 vertPos = getObjectCartesianTransformed(objectType, inPosition);
	gl_Position = projection * view * vec4(vertPos, 1.0);
} 
