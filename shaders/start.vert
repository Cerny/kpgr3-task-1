#version 150

// ------------------------------------------------------
// Variables
// ------------------------------------------------------

in vec2 inPosition; // input from the vertex buffer

uniform mat4 view;
uniform mat4 projection;
uniform mat4 lightViewProjection;
uniform vec3 lightPosition;
uniform vec3 viewPosition; // position of the observation camera
uniform float objectType; // ids of the object types are defined in the ObjectType enum
uniform float time;
uniform mat4 modelTransformation; // transforms object´s position, size and rotation
uniform sampler2D textureBricks;

out vec4 depthTextureCoord;
out vec2 textureCoord;
out vec4 vertexPosition;
out vec3 varyingHalfVector;
out vec3 varyingLightDir; // direction from the vertex to the light source
out vec3 varyingNormal;
out float lightDistance;
out vec4 perVertexColor;

const float PI = 3.1415;

// ------------------------------------------------------
// Functions

// ------------------------------------------------
// ---------------- Get position ------------------
// ------------------------------------------------

// My func 1
vec3 getWavesCartesian(vec2 vec) {
	float s = vec.x;
	float t = vec.y;

	float x = -t;
	float y = s;
	float z = (sin(10 * s + (time / 2)) / 15); // move waves by uniform variable

	return vec3(x, y, z);
}

// My func 2
vec3 getUFOSphere(vec2 vec) {
	float s = (vec.x + 1) * PI; // <-1;1> -> <0;2PI>
	float t = (vec.y + 1) * PI / 2; // <-1;1> -> <0;PI>

	float r = abs(t-PI/2);
	float phi = t*sin(t) + 5;
	float theta = s;

	return vec3(r, phi, theta);
}

// My func 3
vec3 getGlassCylindric(vec2 vec) {
	float t = (vec.x + 1) * PI; // <-1;1> -> <0;2PI>
	float s = (vec.y + 1) * PI; // <-1;1> -> <0;2PI>

	float r = (1+min(sin(t),0))*2;
	float theta = s;
	float z = 3-t;

	return vec3(r, theta, z);
}

vec3 getSombreroCylindric(vec2 vec) {
	float t = (vec.x + 1) * PI; // <-1;1> -> <0;2PI>
	float s = (vec.y + 1) * PI; // <-1;1> -> <0;2PI>

	float r = t;
	float theta = s;
	float z = 2 * sin(t);

	return vec3(r, theta, z);
}

vec3 getEarringSphere(vec2 vec) {
	float s = (vec.x + 1) * PI; // <-1;1> -> <0;2PI>
	float t = (vec.y + 1) * PI / 2; // <-1;1> -> <0;PI>

	float r = abs(t-PI/2);
	float phi = t;
	float theta = s;

	return vec3(r, phi, theta);
}

vec3 getSphere(vec2 vec) {
	float s = PI * 0.5 - PI * ((vec.y + 1) / 2); // <-1;1> -> <PI/2;-PI/2>
	float t = (vec.x + 1) * PI; // <-1;1> -> <0;2PI>
	float r = 0.5;

	float x = r * sin(t) * cos(s);
	float y = r * cos(t) * cos(s);
	float z = r * sin(s);

	return vec3(x, y, z);
}

vec3 getTorus(vec2 vec) {
	float s = vec.x * PI; // <-1;1> -> <-PI;PI>
	float t = vec.y * PI; // <-1;1> -> <-PI;PI>

	float x = (3 * cos(s) + cos(t) * cos(s)) / 4;
	float y = (3 * sin(s) + cos(t) * sin(s)) / 4;
	float z = sin(t) / 4;
	return vec3(x, y, z);
}

vec3 sphereToCartesian(vec3 sphereCoodinations) {
	float r = sphereCoodinations.x;
	float phi = sphereCoodinations.y;
	float theta = sphereCoodinations.z;

	float x = r * sin(phi) * cos(theta);
	float y = r * sin(phi) * sin(theta);
	float z = - r * cos(phi);
	return vec3(x, y, z);
}

vec3 cylindricToCartesian(vec3 sphereCoodinations) {
	float r = sphereCoodinations.x;
	float theta = sphereCoodinations.y;
	float cylinZ = sphereCoodinations.z;

	float x = r * cos(theta);
	float y = r * sin(theta);
	float z = cylinZ;
	return vec3(x, y, z);
}

// Generic function
vec3 getObjectCartesian(float objectType, vec2 position) {
	switch (int(objectType)) {
		case 0: return cylindricToCartesian(getSombreroCylindric(position)) / 10;
		case 1: return sphereToCartesian(getEarringSphere(position)) / 2;
		case 2: return getWavesCartesian(position) / 2;
		case 3: return sphereToCartesian(getUFOSphere(position)) / 3;
		case 4: return cylindricToCartesian(getGlassCylindric(position)) / 6;
		case 5: 							// sphere indicating light
		case 8:							 	// per vertex sphere
		case 9: return getSphere(position); // per pixel sphere
		case 6: return getTorus(position);
		default: return vec3(position, 0); // plane
	}
}

vec3 getObjectCartesianTransformed(float objectType, vec2 position) {
	return (modelTransformation * vec4(getObjectCartesian(objectType, position), 1)).xyz;
}

// ------------------------------------------------
// ---------------- Get normal --------------------
// ------------------------------------------------

// Generic function
vec3 getObjectNormal(float objectType, vec2 position) {
	vec3 u = getObjectCartesianTransformed(objectType, position + vec2(0.001, 0)) - getObjectCartesianTransformed(objectType, position - vec2(0.001, 0));
	vec3 v = getObjectCartesianTransformed(objectType, position + vec2(0, 0.001)) - getObjectCartesianTransformed(objectType, position - vec2(0, 0.001));
	return cross(u, v);
}

// ------------------ main ------------------


void main() {
	textureCoord = inPosition;

	// grid has range <0;1>, transform it to <-1;1>
	vec2 inPosition = inPosition * 2.0 - 1.0;
	vertexPosition =  vec4(getObjectCartesianTransformed(objectType, inPosition), 1);

	vec3 varyingVertPos = vertexPosition.xyz - viewPosition;
	varyingLightDir = lightPosition - vertexPosition.xyz;
	lightDistance = length(varyingLightDir);
	varyingNormal = getObjectNormal(objectType, inPosition);
	varyingHalfVector = (varyingLightDir + (-varyingVertPos)).xyz;
	gl_Position = projection * view * vertexPosition;

	// z position of light
	depthTextureCoord = lightViewProjection * vertexPosition;
	depthTextureCoord.xyz = depthTextureCoord.xyz / depthTextureCoord.w;
	depthTextureCoord.xyz = (depthTextureCoord.xyz + 1) / 2; // screen has range <-1;1>

	if (objectType == 8) { // per vertex surface
		perVertexColor = texture(textureBricks, textureCoord);
	}
} 
