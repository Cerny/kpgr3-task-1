package main;

public enum SurfaceType {
    depthColor(0), color(1), texture(2), normal(3), coordinateToTexture(4),
    xyzPosition(5);
    public final float id;

    private SurfaceType(Integer number) {
        this.id = (float) number;
    }
    public static String name(int id) {
        switch (id) {
            case 0: return "Depth color";
            case 1: return "Color";
            case 2: return "Texture";
            case 3: return "Normal";
            case 4: return "Coordinate to texture";
            case 5: return "XYZ position";
            default: return "";
        }
    }
}