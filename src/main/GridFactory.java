package main;

import lwjglutils.OGLBuffers;

public class GridFactory {

    public static OGLBuffers generateGrid(int xSize, int ySize) {
        int[] ib = createIB(xSize, ySize);
        float[] vb = createVB(xSize, ySize);

        OGLBuffers.Attrib[] attributes = {
                new OGLBuffers.Attrib("inPosition", 2)
        };
        return new OGLBuffers(vb, attributes, ib);
    }

    static private int[] createIB(int xSize, int ySize) {
        int[] ib = new int[(ySize - 1) * (xSize - 1) * 6];
        int index = 0;
        for (int x = 0; x < xSize - 1; x++) {
            for (int y = 0; y < ySize - 1; y++) {
                ib[index++] = y + (x * ySize);
                ib[index++] = y + 1 + (x * ySize);
                ib[index++] = y + ySize + (x * ySize);

                ib[index++] = y + ySize + (x * ySize);
                ib[index++] = y + 1 + (x * ySize);
                ib[index++] = y + ySize + 1 + (x * ySize);
            }
        }
        return ib;
    }

    static private float[] createVB(int xSize, int ySize) {
        float[] vb = new float[xSize * ySize * 2];
        int index = 0;
        for (int x = 0; x < xSize; x++) {
            for (int y = 0; y < ySize; y++) {
                vb[index++] = x / (float) (xSize - 1);
                vb[index++] = y / (float) (ySize - 1);
            }
        }
        return vb;
    }

}
