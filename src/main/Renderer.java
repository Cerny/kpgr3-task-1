package main;

import lwjglutils.*;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import transforms.*;

import java.awt.*;
import java.io.IOException;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11C.glClearColor;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;

/**
 * @author Michal Černý & PGRF FIM UHK
 * @since 2020-04-09
 */
public class Renderer extends AbstractRenderer {

    private int shaderProgramViewer, shaderProgramLight;
    // Render from view - shader properties
    private int locView, locProjection, locType, locTime, locLightPosition, locLightViewProjection, locViewPos, locModelTransformation,
            locLightCutoffAngle, locLightDirection, locLightMode;
    // Render from viewer - shader properties
    private int locViewLight, locProjectionLight, locTypeLight, locTimeLight, locModelTransformationLight;
    // decides whether render front sides and back sides of triangles
    private boolean renderFront = true, renderBack = true;
    // ids of the surface types are defined in the SurfaceType enum
    private int locSurfaceType = (int) SurfaceType.texture.id, surfaceType = locSurfaceType;
    // defines whether light camera or view camera is currently moving (by keyboard or mouse)
    private boolean movingLight = false;

    private float presentingObject = SceneObject.waves.id;
    private boolean showingPerVertexPerPixelDiff = false;
    private float lightMode = 1; // 1: Blinn-Phong and attenuation is on, 2: Blinn-Phong and attenuation is off
    private boolean showingDebugViews = false; // show views with depth

    // Properties for moving torus and light
    double torusR = 3;
    // If torusR = 3 and moveLength = 0.3 then it takes 10 times to move light from the center to the torus edge.
    // Conditions for moveLength: torusR % moveLength = 0, moveLength < torusR
    float moveLength = 0.018f;
    float torusRotation = 0.0f;
    boolean lightIsMovingBackwards = true;
    boolean rotate = true;

    private OGLBuffers buffers;
    private Camera camera, cameraLight;
    private Mat4 projection;
    private OGLTexture2D textureBricks, textureSea;
    private OGLTexture2D.Viewer viewer;
    private OGLRenderTarget renderTarget;

    Mat4 projectionPerspective = new Mat4PerspRH(Math.PI / 3, LwjglWindow.HEIGHT/(float)LwjglWindow.WIDTH, 2, 60);
    Mat4 projectionOrthogonal = new Mat4OrthoRH( 10, 10, 2, 60);
    Boolean perspective = true;

    // variable used to move objects (waves in this case). Every time the display() is called, the time is increased
    private float time = 0;

    // Constants
    private int textSize = 15;
    private float lightCutoffAngle = (float) Math.PI / 6;
    private float cameraStep = 0.5f; // length of move when controlling camera position by keyboard

    @Override
    public void init() {
        OGLUtils.printOGLparameters();
        OGLUtils.printLWJLparameters();
        OGLUtils.printJAVAparameters();
        OGLUtils.shaderCheck();

        glClearColor(0.1f, 0.1f, 0.1f, 0.0f);
        textRenderer = new OGLTextRenderer(width, height, new Font("SansSerif", Font.PLAIN, textSize));
        glEnable(GL_DEPTH_TEST); // enable z-test (z-buffer) - after new OGLTextRenderer
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // fill both front and back sides

        shaderProgramViewer = ShaderUtils.loadProgram("/start");
        shaderProgramLight = ShaderUtils.loadProgram("/light");

        // Viewer shader
        locView = glGetUniformLocation(shaderProgramViewer, "view");
        locProjection = glGetUniformLocation(shaderProgramViewer, "projection");
        locType = glGetUniformLocation(shaderProgramViewer, "objectType");
        locTime = glGetUniformLocation(shaderProgramViewer, "time");
        locLightPosition = glGetUniformLocation(shaderProgramViewer, "lightPosition");
        locLightViewProjection = glGetUniformLocation(shaderProgramViewer, "lightViewProjection");
        locSurfaceType = glGetUniformLocation(shaderProgramViewer,"surfaceType");
        locViewPos = glGetUniformLocation(shaderProgramViewer, "viewPosition");
        locModelTransformation = glGetUniformLocation(shaderProgramViewer, "modelTransformation");
        locLightCutoffAngle = glGetUniformLocation(shaderProgramViewer, "lightCutoffAngle");
        locLightDirection = glGetUniformLocation(shaderProgramViewer, "lightDirection");
        locLightMode = glGetUniformLocation(shaderProgramViewer, "lightMode");

        // Light shader
        locViewLight = glGetUniformLocation(shaderProgramLight, "view");
        locProjectionLight = glGetUniformLocation(shaderProgramLight, "projection");
        locTypeLight = glGetUniformLocation(shaderProgramLight, "objectType");
        locTimeLight = glGetUniformLocation(shaderProgramLight, "time");
        locModelTransformationLight = glGetUniformLocation(shaderProgramLight, "modelTransformation");

        renderTarget = new OGLRenderTarget(1024, 1024);

        buffers = GridFactory.generateGrid(100, 100);

        cameraLight = new Camera()
                .withPosition(new Vec3D(0, 0, 0))
                .withAzimuth(Math.PI / 2)
                .withZenith(0);

        camera = new Camera()
                .withPosition(new Vec3D(-5.69,-0.76,4.08))
                .withAzimuth(0.56)
                .withZenith(-0.71);

        projection = new Mat4PerspRH(
                Math.PI / 3,
                LwjglWindow.HEIGHT / (float) LwjglWindow.WIDTH,
                1,
                20
        );

        viewer = new OGLTexture2D.Viewer();
        try {
            textureBricks = new OGLTexture2D("./textures/bricks.jpg");
            textureSea = new OGLTexture2D("./textures/sea.jpg");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // If rotate variable is true, torus is rotating and light is moving forward and backwards.
    // Properties for this movement are set in this function.
    private void setupMovingObjectsProperties() {
        if (!rotate) {
            return;
        }

        if (lightIsMovingBackwards && cameraLight.getPosition().getY() <= -torusR) {
            lightIsMovingBackwards = false; // Light is gonna move to the left now
        } else if (!lightIsMovingBackwards && cameraLight.getPosition().getY() >= torusR) {
            lightIsMovingBackwards = true; // Light is gonna move to the right now
        }

        if (lightIsMovingBackwards) {
            cameraLight = cameraLight.backward(moveLength);
        } else {
            cameraLight = cameraLight.forward(moveLength);
        }
        torusRotation += Math.PI/2 * (moveLength/torusR);
    }

    @Override
    public void display() {
        setupMovingObjectsProperties();
        updateSettingsAndTextRenderer();

        time += 0.1;

        renderFromLight();
        renderFromViewer();

        textRenderer.draw();

        if (showingDebugViews) {
            viewer.view(renderTarget.getColorTexture(), -1, 0, 0.5);
            viewer.view(renderTarget.getDepthTexture(), -1, -0.5, 0.5);
            viewer.view(textureBricks, -1, -1, 0.5);
        }
    }

    // Helper function for updateSettingsAndSetupTextRenderer. Returns new yOffset.
    private int addStringToTextRenderer(OGLTextRenderer textRenderer, int yOffset, String text) {
        int yStepOffset = textSize + 3;
        int xOffset = 15;

        textRenderer.addStr2D(xOffset, yOffset, text);
        return yOffset + yStepOffset;
    }

    private void updateSettingsAndTextRenderer() {
        int yOffset = 30;
        textRenderer.clear();

        yOffset = addStringToTextRenderer(textRenderer, yOffset, "camera [WASD, SHIFT, CTRL], first person [SPACE]");
        yOffset = addStringToTextRenderer(textRenderer, yOffset, "[m]ove to light position");
        yOffset = addStringToTextRenderer(textRenderer, yOffset, "moving [l]ight / camera: " + (movingLight ? "light" : "camera"));
        yOffset = addStringToTextRenderer(textRenderer, yOffset, "[q] show difference between per-vertex and per-pixel surface computation. ");

        if (showingPerVertexPerPixelDiff) {
            yOffset = addStringToTextRenderer(textRenderer, yOffset, "   Sphere on the left side from the light point of view has surface computed per-pixel (in fragment shader).");
            yOffset = addStringToTextRenderer(textRenderer, yOffset, "   Sphere on the right side has surface computed per-vertex (in vertex shader).");
            yOffset = addStringToTextRenderer(textRenderer, yOffset, "   Lightning is not taken into account in this case.");
        }

        if (renderFront)    { glPolygonMode(GL_FRONT, GL_FILL); }
        else                { glPolygonMode(GL_FRONT, GL_LINE); }
        yOffset = addStringToTextRenderer(textRenderer, yOffset, "[f]ront polygons: " + (renderFront ? "fill" : "line"));

        if (renderBack) { glPolygonMode(GL_BACK, GL_FILL); }
        else            { glPolygonMode(GL_BACK, GL_LINE); }
        yOffset = addStringToTextRenderer(textRenderer, yOffset, "[b]ack polygons: " + (renderBack ? "fill" : "line"));

        yOffset = addStringToTextRenderer(textRenderer, yOffset, "[r]otate: " + (rotate ? "true" : "false"));
        yOffset = addStringToTextRenderer(textRenderer, yOffset, "surface [t]ype: " + SurfaceType.name(surfaceType));
        yOffset = addStringToTextRenderer(textRenderer, yOffset, "[p]erspective: " + (perspective ? "perspective" : "orthogonal"));
        yOffset = addStringToTextRenderer(textRenderer, yOffset, "show d[e]bug views: " + (showingDebugViews ? "true" : "false"));
        yOffset = addStringToTextRenderer(textRenderer, yOffset, "print [c]amera info to console");
        yOffset = addStringToTextRenderer(textRenderer, yOffset, "lig[h]t mode: " + lightMode);
        addStringToTextRenderer(textRenderer, yOffset, "[o]bject type: " + SceneObject.name((int) presentingObject));
    }

    private void renderFromLight() {
        glUseProgram(shaderProgramLight);
        renderTarget.bind();

        glClearColor(0, 0.5f, 0, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUniformMatrix4fv(locViewLight, false, cameraLight.getViewMatrix().floatArray());
        glUniformMatrix4fv(locProjectionLight, false, projection.floatArray());
        glUniform1f(locTimeLight, time);

        drawObjects(shaderProgramLight, locModelTransformationLight, locTypeLight);
    }

    private void renderFromViewer() {
        glUseProgram(shaderProgramViewer);

        // teacher's note: need to fix viewport because render target set its own
        glViewport(0, 0, width, height);

        // default framebuffer - render to the screen
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        glClearColor(0.1f, 0.1f, 0.1f, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUniformMatrix4fv(locView, false, camera.getViewMatrix().floatArray());
        glUniformMatrix4fv(locProjection, false, projection.floatArray());
        glUniform3fv(locLightPosition, ToFloatArray.convert(cameraLight.getPosition()));
        glUniformMatrix4fv(locLightViewProjection, false, cameraLight.getViewMatrix().mul(projection).floatArray());
        glUniform1f(locTime, time);
        glUniform1f(locSurfaceType, surfaceType);
        glUniform3f(locViewPos, (float) camera.getPosition().getX(), (float) camera.getPosition().getY(), (float) camera.getPosition().getZ());
        glUniform1f(locLightCutoffAngle, (float) Math.cos(lightCutoffAngle));
        glUniform3f(locLightDirection, (float)cameraLight.getViewVector().getX(), (float)cameraLight.getViewVector().getY(), (float)cameraLight.getViewVector().getZ());
        glUniform1f(locLightMode, lightMode);

        renderTarget.getDepthTexture().bind(shaderProgramViewer, "depthTexture", 1);
        textureBricks.bind(shaderProgramViewer, "textureBricks", 0);
        textureSea.bind(shaderProgramViewer, "textureSea", 2);

        // setup scene projection
        glUniformMatrix4fv(locProjection, false, (perspective ? projectionPerspective : projectionOrthogonal).floatArray());

        // Light - it is drawn just in the render from viewer, so that´s why it is not separated from the drawObjets().
        glUniform1f(locType, SceneObject.light.id);
        glUniformMatrix4fv(locModelTransformation, false, getModelTransformationMatrix(0.1, cameraLight.getPosition()).floatArray());
        buffers.draw(GL_TRIANGLES, shaderProgramViewer);

        drawObjects(shaderProgramViewer, locModelTransformation, locType);
    }

    void drawObjects(int shaderProgram, int locModelTransformationShader, int locTypeShader) {
        // If showing per-vertex and per-pixel difference is true, just two spheres are presented. Otherwise show standard objects
        if (showingPerVertexPerPixelDiff) {
            // This sphere's surface is computed in fragment shader
            glUniform1f(locType, SceneObject.spherePerPixel.id);
            glUniformMatrix4fv(locModelTransformation, false, getModelTransformationMatrix(3., new Vec3D(-2,5,-1)).floatArray());
            buffers.draw(GL_TRIANGLES, shaderProgramViewer);

            // This sphere's surface is computed in vertex shader
            glUniform1f(locType, SceneObject.spherePerVertex.id);
            glUniformMatrix4fv(locModelTransformation, false,  getModelTransformationMatrix(3., new Vec3D(2,5,-1)).floatArray());
            buffers.draw(GL_TRIANGLES, shaderProgramViewer);
        } else {
            // User choose which object will be presented
            glUniform1f(locTypeShader, presentingObject);
            glUniformMatrix4fv(locModelTransformationShader, false, getModelTransformationMatrix(2., new Vec3D(0, 5,-1)).floatArray());
            buffers.draw(GL_TRIANGLES, shaderProgram);

            // Torus
            glUniform1f(locTypeShader, SceneObject.torus.id);
            glUniformMatrix4fv(locModelTransformationShader, false, new Mat4RotX(torusRotation).mul(getModelTransformationMatrix(torusR, new Vec3D(0, 0, 0))).floatArray());
            buffers.draw(GL_TRIANGLES, shaderProgram);

            // Plane
            glUniform1f(locTypeShader, SceneObject.plane.id);
            glUniformMatrix4fv(locModelTransformationShader, false, getModelTransformationMatrix(25., new Vec3D(0, 0, -3)).floatArray());
            buffers.draw(GL_TRIANGLES, shaderProgram);
        }
    }

    Mat4 getModelTransformationMatrix(Double scale, Vec3D offset) {
        return new Mat4Scale(scale).mul(new Mat4Transl(offset.getX(), offset.getY(), offset.getZ()));
    }

    @Override
    public GLFWCursorPosCallback getCursorCallback() {
        return cursorPosCallback;
    }

    @Override
    public GLFWMouseButtonCallback getMouseCallback() {
        return mouseButtonCallback;
    }

    @Override
    public GLFWKeyCallback getKeyCallback() {
        return keyCallback;
    }

    @Override
    public GLFWWindowSizeCallback getWsCallback() {
        return wsCallback;
    }

    private double oldMx, oldMy;
    private boolean mousePressed;

    private GLFWCursorPosCallback cursorPosCallback = new GLFWCursorPosCallback() {
        @Override
        public void invoke(long window, double x, double y) {
            if (mousePressed) {
                Camera newCamera = (movingLight ? cameraLight : camera).addAzimuth(Math.PI / 2 * (oldMx - x) / LwjglWindow.WIDTH)
                        .addZenith(Math.PI / 2 * (oldMy - y) / LwjglWindow.HEIGHT);
                if (movingLight)      { cameraLight = newCamera; }
                else                { camera = newCamera; }
                oldMx = x;
                oldMy = y;
            }
        }
    };

    private GLFWMouseButtonCallback mouseButtonCallback = new GLFWMouseButtonCallback() {
        @Override
        public void invoke(long window, int button, int action, int mods) {
            if (button == GLFW_MOUSE_BUTTON_LEFT) {
                double[] xPos = new double[1];
                double[] yPos = new double[1];
                glfwGetCursorPos(window, xPos, yPos);
                oldMx = xPos[0];
                oldMy = yPos[0];
                mousePressed = action == GLFW_PRESS;
            }
        }
    };

    private GLFWKeyCallback keyCallback = new GLFWKeyCallback() {
        @Override
        public void invoke(long window, int key, int scancode, int action, int mods) {
            {
                if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
                    glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
                if (action == GLFW_PRESS || action == GLFW_REPEAT) {
                    switch (key) {
                        case GLFW_KEY_W:
                            if (movingLight)  { cameraLight = cameraLight.forward(cameraStep); }
                            else            { camera = camera.forward(cameraStep); }
                            break;
                        case GLFW_KEY_D:
                            if (movingLight)  { cameraLight = cameraLight.right(cameraStep); }
                            else            { camera = camera.right(cameraStep); }
                            break;
                        case GLFW_KEY_S:
                            if (movingLight)  { cameraLight = cameraLight.backward(cameraStep); }
                            else            { camera = camera.backward(cameraStep); }
                            break;
                        case GLFW_KEY_A:
                            if (movingLight)  { cameraLight = cameraLight.left(cameraStep); }
                            else            { camera = camera.left(cameraStep); }
                            break;
                        case GLFW_KEY_LEFT_CONTROL:
                            if (movingLight)  { cameraLight = cameraLight.down(cameraStep); }
                            else            { camera = camera.down(cameraStep); }
                            break;
                        case GLFW_KEY_LEFT_SHIFT:
                            if (movingLight)  { cameraLight = cameraLight.up(cameraStep); }
                            else            { camera = camera.up(cameraStep); }
                            break;
                        case GLFW_KEY_SPACE:
                            camera = camera.withFirstPerson(!camera.getFirstPerson());
                            break;
                        case GLFW_KEY_R:
                            rotate = !rotate;
                            break;
                        case GLFW_KEY_F:
                            renderFront = !renderFront;
                            break;
                        case GLFW_KEY_B:
                            renderBack = !renderBack;
                            break;
                        case GLFW_KEY_P:
                            perspective = !perspective;
                            break;
                        case GLFW_KEY_T:
                            if (surfaceType < 5) {
                                surfaceType += 1;
                            } else {
                                surfaceType = 0;
                            }
                            break;
                        case GLFW_KEY_L:
                            movingLight = !movingLight;
                            break;
                        case GLFW_KEY_O:
                            if (presentingObject < 4) {
                                presentingObject += 1;
                            } else {
                                presentingObject = 0;
                            }
                            break;
                        case GLFW_KEY_M:
                            camera = cameraLight;
                            break;
                        case GLFW_KEY_C:
                            System.out.println("Camera: " + camera.toString());
                            System.out.println("Camera light: " + cameraLight.toString());
                            break;
                        case GLFW_KEY_Q:
                            showingPerVertexPerPixelDiff = !showingPerVertexPerPixelDiff;
                            break;
                        case GLFW_KEY_H:
                            lightMode = lightMode == 1 ? 2 : 1;
                            break;
                        case GLFW_KEY_E:
                            showingDebugViews = !showingDebugViews;
                            break;
                    }
                }
            }
        }
    };

    private GLFWWindowSizeCallback wsCallback = new GLFWWindowSizeCallback() {
        @Override
        public void invoke(long window, int w, int h) {
            if (w > 0 && h > 0 && (w != width || h != height)) {
                width = w;
                height = h;
                projectionPerspective = new Mat4PerspRH(Math.PI / 3, height / (double) width, 2, 60);

                if (textRenderer != null)
                    textRenderer.resize(width, height);
            }
        }
    };

}
