package main;

public enum SceneObject {
    // Objects 0-4 are not presented in one time, just one of them is. Which one defines the presentingObject variable.
    sombrero(0), earring(1), waves(2), UFO(3), glass(4),
    // Objects that are always presented:
    light(5), torus(6), plane(7), spherePerVertex(8), spherePerPixel(9);
    public final float id;
    private SceneObject(Integer number) {
        this.id = (float) number;
    }
    public static String name(int id) {
        switch (id) {
            case 0: return "Sombrero";
            case 1: return "Earring";
            case 2: return "Waves";
            case 3: return "UFO";
            case 4: return "Glass";
            case 5: return "Light";
            case 6: return "Torus";
            case 7: return "Plane";
            default: return "";
        }
    }
}